
const draggableDiv = document.getElementById('draggable');

draggableDiv.addEventListener('mousedown', onMouseDown);
draggableDiv.addEventListener('mouseup', onMouseUp);

let xStart;
let yStart;

function onMouseDown(event) {
	const rect = draggableDiv.getBoundingClientRect();

	draggableDiv.style.position = 'fixed';
	draggableDiv.style.zIndex = 9999;

	xStart = event.pageX - rect.left;
	yStart = event.pageY - rect.top;

	document.addEventListener('mousemove', onMouseMove);
}

function onMouseMove(event) {
	const { pageX, pageY } = event;
	draggableDiv.style.left = (pageX - xStart) + 'px';
	draggableDiv.style.top = (pageY - yStart) + 'px';
}

function onMouseUp(event) {
	document.removeEventListener('mousemove', onMouseMove);
}