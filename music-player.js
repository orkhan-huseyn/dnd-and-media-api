
const musicList = [
	{
		artist: 'Engelbert Humperdinck',
		title: 'A Man Without Love',
		src: 'audio/a_man_without_love.mp3',
	},
	{
		artist: 'Ahmed Saad',
		title: 'El Melouk',
		src: 'audio/el_melouk.mp3',
	},
	{
		artist: 'Maxence Cyrin',
		title: 'Where Is My Mind',
		src: 'audio/where_is_my_mind.mp3',
	},
];

const music = document.getElementById('music');
const prev = document.getElementById('prev');
const next = document.getElementById('next');
const currentSpan = document.getElementById('current');
const durationSpan = document.getElementById('duration');
const progressContainer = document.getElementById('progressContainer');
const progress = document.getElementById('progress');
const ball = document.getElementById('ball');
const artistBox = document.getElementById('artistBox');

const play = document.getElementById('play');

music.controls = false;

let currentIndex = 0;
let isPlaying = false;

function loadMusic(index) {
	const currentMusic = musicList[index];
	artistBox.textContent = `${currentMusic.title} by ${currentMusic.artist}`;
	music.src = currentMusic.src;
	setTimeout(setDurationTime, 500);
}

loadMusic(currentIndex);

prev.addEventListener('click', function() {
	currentIndex--;
	if (currentIndex < 0) {
		currentIndex = musicList.length - 1;
	}
	loadMusic(currentIndex);
});

next.addEventListener('click', function() {
	currentIndex++;
	if (currentIndex > musicList.length - 1) {
		currentIndex = 0;
	}
	loadMusic(currentIndex);
});

play.addEventListener('click', function() {
	if (isPlaying) {
		pauseMusic();
	} else {
		playMusic();
	}
});

function playMusic() {
	music.play();
	isPlaying = true;
	play.textContent = 'Pause';
}

function pauseMusic() {
	music.pause();
	isPlaying = false;
	play.textContent = 'Play';
}

music.addEventListener('timeupdate', function (event) {
	const { currentTime, duration } = event.target;

	const elapsedMinutes = Math.floor(currentTime / 60).toString().padStart(2, '0');
	const elapsedSeconds = Math.floor(currentTime % 60).toString().padStart(2, '0');
	const elapsedTime = elapsedMinutes + ':' + elapsedSeconds;

	currentSpan.textContent = elapsedTime;

	const elapsedPertentage = (currentTime / duration ) * 100;
	progress.style.width = elapsedPertentage + '%';
	ball.style.left = elapsedPertentage - 1 + '%';
});

progressContainer.addEventListener('click', function (event) {
	const offset = event.offsetX;
	const width = progressContainer.clientWidth;

	const percentage = offset / width;
	music.currentTime = music.duration * percentage;
});

function setDurationTime() {
	const durationMinutes = Math.floor(music.duration / 60).toString().padStart(2, '0');
	const durationSeconds = Math.floor(music.duration % 60).toString().padStart(2, '0');
	const durationTime = durationMinutes + ':' + durationSeconds;

	durationSpan.textContent = durationTime;
}