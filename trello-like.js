
const addTodoButton = document.getElementById('addBtn');
const modal = document.getElementById('todoForm');
const overlay = document.getElementById('overlay');
const closeModalButton = document.querySelector('.close-modal');
const addTodoForm = document.getElementById('addTodoForm');
const backlog = document.getElementById('noStatus');

let draggedElement;

addTodoButton.addEventListener('click', openModal);
closeModalButton.addEventListener('click', closeModal);

addTodoForm.addEventListener('submit', function (event) {
	event.preventDefault();
	const { todoText } = addTodoForm.elements;
	
	const todoDiv = document.createElement('div');
	todoDiv.classList.add('todo');
	todoDiv.draggable = true;
	todoDiv.append(document.createTextNode(todoText.value));

	todoDiv.addEventListener('dragstart', onDragStart);
	todoDiv.addEventListener('dragend', onDragEnd);

	backlog.append(todoDiv);
	closeModal();
	todoText.value = "";
});

function openModal() {
	modal.classList.add('active');
	overlay.classList.add('active');
}

function closeModal() {
	modal.classList.remove('active');
	overlay.classList.remove('active');
}

const columns = document.querySelectorAll('.status');
const todo = document.querySelector('.todo');

columns.forEach(function(column) {
	column.addEventListener('dragover', onDragOver);
	column.addEventListener('drop', onDrop);
	column.addEventListener('dragenter', onDragEnter);
	column.addEventListener('dragleave', onDragLeave);
});

function onDragLeave(event) {
	event.currentTarget.style.border = 'none';
}

function onDragEnter(event) {
	event.currentTarget.style.border = '5px dashed gray';
}

function onDragOver(event) {
	event.preventDefault();
}

function onDrop(event) {
	event.currentTarget.append(draggedElement);
	event.currentTarget.style.border = 'none';
}

function onDragStart(event) {
	draggedElement = event.currentTarget;
}

function onDragEnd() {
	draggedElement = null;
}